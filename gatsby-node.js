exports.onCreateWebpackConfig = ({
  stage,
  rules,
  loaders,
  plugins,
  actions,
}) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /emailjs-com/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
  actions.setWebpackConfig({
    module: {

    },
    resolve: {
      modules: [
        "node_modules",
        'src'
      ],
      extensions: ['*', '.js', '.jsx', '.scss', '.css']
    },

    node: { fs: 'empty' },

  })
}