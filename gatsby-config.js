/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [`gatsby-plugin-sass`, {
    resolve: `gatsby-plugin-s3`,
    options: {
      bucketName: "wz-media-bucket",
      acl:null
    },
  },],

}
