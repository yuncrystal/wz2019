import React, { useState } from "react"
import PropTypes from 'prop-types';
import HeaderModal from './headerModal';

const HeaderMarket = ({ img }) => {


    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };



    return (
        <div className='header-container'>
            <img src={img} alt="banner" />
            <div className='header-inner'>
                <div className='menu-row'>
                    <div className='logo'><img src="/img/logo.png" alt="menu.png" /></div>
                    <div className='menu' onClick={handleOpen}><img src="/img/icon/menu.png" alt="menu.png" /></div>
                </div>
                <div className='title'>
                    <div className='flex-end'>
                        <div>MARKETING</div>
                        <div className='line'></div>
                    </div>
                    <div>CREATIVE DESIGN</div>
                </div>
            </div>
            <div className='triangle-container'>
                <div className='triangle'>
                    <img src="/img/icon/Polygon.png" alt=""/>
                </div>
            </div>
            <HeaderModal img={img} open={open} handleClose={handleClose} />
        </div>

    )
}

HeaderMarket.propTypes = {
    img: PropTypes.string.isRequired,
};
export default HeaderMarket;