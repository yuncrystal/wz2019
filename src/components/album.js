import React from "react"
import Box from '@material-ui/core/Box';
import {Link} from 'gatsby'
import "./album.scss";

const AlbumItem = ({ url,link }) => {
    return (
        <Link to={link} className='album-colum'>
            <img src={url} alt="" />
            <div className='abs-container'>
            </div>
        </Link>
    )
}

const Album = () => {

    return (
        <Box className='container album-slider-container'>
            <Box className='row row-margin album-row'>
                <AlbumItem link='/king-tea' url={'/img/album-group-1.png'} />
                <AlbumItem link='/singer-2018' url={'/img/album-group-2.png'} />
                <AlbumItem link='/blackmores' url={'/img/album-group-3.png'} />
                <AlbumItem link='/bayer' url={'/img/album-group-4.png'} />
            </Box>
        </Box>

    )
}


export default Album;