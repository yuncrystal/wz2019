import React from "react"
import Box from '@material-ui/core/Box';
import { Link } from "gatsby"
import "./footer.scss";


const Footer = ({ }) => {

    return (
        <Box className='footer-container'>
            <Box className='touch'>
                <div className='title title5 c-white font-light'>Start a Project</div>
                <div className='question title6 c-white font-light'>Like what you see? We’d love to hear form you!</div>
                <Link to='/contact' className='touch-btn title7 font-light flex-center c-white' style={{color:'#fff'}}>Get In Touch</Link>
            </Box>

            <Box className='links-container c-white bg-grey2'>
                <Box className='column-link'>
                    <div className='logo-zh'>
                        <img src="/img/logo-zh.png" alt="" />
                    </div>
                    <div className='content6 margin-text'>
                        Headquarters:<br />
                        Unit9/30, Maddox St,<br />
                        Alexandria, Sydney
                    </div>
                    <div className='content6 margin-text'>
                        Phone: 0487 880 136<br />
                        Email: info@wzm.com.au
                    </div>
                </Box>

                <Box className='column-link'>
                    <div className='content5 margin-text'>
                        友情链接
                    </div>
                    <div className='content6 margin-text'>
                        Buttler Organics
                    </div>

                    <div className='content6 margin-text'>
                        OLI6
                    </div>

                    <div className='content6 margin-text'>
                        Kotia
                    </div>
                </Box>

                <Box className='column-link'>
                    <div className='content5 margin-text'>
                        最新项目
                    </div>
                    <div className='content6 margin-text'>
                        KING TEA x BUTLER<br />
                        ORGANICS EVENT
                    </div>

                    <div className='content6 margin-text'>
                        oli6颖睿羊奶粉<br />
                        Brand Day
                    </div>
                </Box>

                <Box className='column-contact'>
                    <div className='content5 margin-text'>
                        关注我们
                    </div>

                    <div className='content6'>
                        Subscribe to Our Newsletter to get Important News,<br />
                        Amazing Offers & Inside Scoops:
                    </div>

                    <div className='subscribe'>
                        <input type="text" placeholder='ENTER YOUR EMAIL' />
                        <div className='subscribe-btn flex-center'>SUBSCRIBE</div>
                    </div>
                </Box>
            </Box>

            <Box className='links-container c-white bg-black2 content6 '>
                <div className='c-grey'>
                    Copyrights © 2018 All Rights Reserved by WZmedia.<br />
                    Terms of Use / Privacy Policy
                </div>
                <div className='c-grey'>
                    info@wzm.com.au ·  02 8540 7883
                </div>

            </Box>
        </Box>

    )
}


export default Footer;