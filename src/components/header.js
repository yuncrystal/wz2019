import React, { useState } from "react"
import PropTypes from 'prop-types';
import HeaderModal from './headerModal';
import {Link} from 'gatsby'

const Header = ({ img, title, titleClass }) => {


    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className='header-container'>
            <img src={img} alt="banner" />
            <div className='header-inner'>
                <div className='menu-row'>
                    <Link className='logo' to='/'><img src="/img/logo.png" alt="menu.png" /></Link>
                    <div className='menu' onClick={handleOpen}><img src="/img/icon/menu.png" alt="menu.png" /></div>
                </div>
                <div className={titleClass}>{title}</div>
            </div>
            <div className='triangle-container'>
                <div className='triangle'>
                    <img src="/img/icon/Polygon.png" alt="" />
                </div>
            </div>
            <HeaderModal img={img} open={open} handleClose={handleClose} />
        </div>

    )
}

Header.defaultProps = {
    titleClass: 'title-bg',
};

Header.propTypes = {
    img: PropTypes.string.isRequired,
    titleClass: PropTypes.string.isRequired,
};
export default Header;