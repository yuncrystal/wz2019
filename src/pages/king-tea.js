import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import { Link } from "gatsby"
import "./singer.scss";


export default () => {
    return (
        <div>
            <Header title={'King Tea'} img={'/img/tea-banner.png'} />

            <Box className='container singer'>
                <Box className='section3 content7 describe'>
                    KING TEA 皇茶与BUTLER ORGANICS 跨界合作七夕活动：KING TEA 皇茶与BUTLER ORGANICS 洗发水跨界合作发布会以及七夕活动，邀请了许多在悉尼本地的媒体代表及网红小姐姐们参加，在七夕当晚唐人街范围内进行了几千⼈次的互动宣传。
                </Box>

                <div className='row-margin-3'>
                    <img src="/img/tea-1.png" alt="" />
                </div>

                <div>
                    <img src="/img/tea-2.png" alt="" />
                </div>

                <Box className='bg-grey c-white'>
                    <Box className='flex-center describe'>
                        <Box className='content7'>
                        我是站位福
                        </Box>

                    </Box>
                </Box>


                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/tea-3.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/tea-4.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/tea-5.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/tea-6.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/tea-7.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/tea-8.png" alt="" />
                    </div>
                </Box>
            </Box>

            <Album />
            <Footer />

        </div>
    )
}