import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import { Link } from "gatsby"
import "./singer.scss";

export default () => {
    return (
        <div>
            <Header titleClass={'title-mid'} title={'The Singr 2018'} img={'/img/singer2-banner.png'} />
            <Box className='container singer'>
                <div className='row years'>
                    <Link to="/singer-2017" className='item title1'>2017</Link>
                    <div className='item title1 active'>2018</div>
                    <Link to="/singer-2019" className='item title1'>2019</Link>
                </div>

                <Box className='section2 row'>
                    <div className='colum2'>
                        <img src="/img/singer2-1.png" alt="" />
                    </div>
                    <div className='colum2 flex-center'>
                        <div className='title half-content content7'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                        </div>
                    </div>
                </Box>

                <div>
                    <img src="/img/singer2-2.png" alt="" />
                </div>

                <Box className='section3 content7 describe'>
                    我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                </Box>


                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer2-3.png" alt="" />
                    </div>

                    <div className='abs-container title1 font-light c-white'>
                        The Singer 2018 press conference
                    </div>
                </Box>

                <Box className='bg-grey c-white'>
                    <Box className='describe flex-center'>
                        <Box className='content7'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                        </Box>

                    </Box>
                </Box>

                <Box className='section1 row'>
                    <div className='colum2'>
                        <img src="/img/singer2-4.png" alt="" />
                    </div>
                    <div className='colum2 flex-center'>
                        <div className='content7'>我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福</div>
                    </div>
                </Box>


                <Box className='section7'>
                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/singer2-5.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer2-6.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row'>
                        <div className='colum2'>
                            <img src="/img/singer2-7.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer2-8.png" alt="" />
                        </div>
                    </Box>
                </Box>

                <Box className='white-block2'></Box>

                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer2-9.png" alt="" />
                    </div>
                    <div className='abs-container title1 flex-end font-light c-white'>
                        The Singer 2018 tutor event
                    </div>
                </Box>


                <Box className='bg-grey c-white'>
                    <Box className='flex-center describe'>
                        <Box className='content7'>
                            「Blackmores 澳佳宝」独家冠名 2018 the singer 唱响星途全球华人流行歌手大赛 澳洲区总决赛在5月27日傍晚，正式结束，在激烈的角逐后，2018唱响星途全球华人流行歌手大赛澳洲赛区的十组即将代表澳洲赛区参加全球十二强总决赛的选手已经选出。这十组澳洲战队选手将一起参加为期一个月的培训，在培训期结束后，澳洲赛区才会确定参加全球十二强总决赛之夜与其他赛区选手现场pk的澳洲赛区首发选手。 本次唱响星途全球总决赛将迎来五位拥有国际知名度的明星导师.
                        </Box>

                    </Box>
                </Box>

                <Box className='tutors'>
                    <Box className='line'>
                        <Box className='block'>
                            <div>
                                <img src="/img/tutor-1.png" alt="" />
                            </div>
                            <div className='name'>郭峰</div>
                            <div className='brief'>
                                原创音乐大师- 郭峰，中国原创音乐开会发起第一人，2008北京申办奥运会音乐第一人，被“东方时空”誉为东方之子的流行音乐第一人，集词、曲、编曲、制作、演奏、演唱、策划、导演于一身的全方位音乐人。代表作品有《让世界充满爱》、《心会跟爱一起走》、《心愿》等。
                            </div>
                        </Box>

                        <Box className='block'>
                            <div>
                                <img src="/img/tutor-2.png" alt="" />
                            </div>
                            <div className='name'>六小龄童</div>
                            <div className='brief'>
                                表演艺术大师- 六小龄童，本名章金莱，1959年4月12日生于中国上海，祖籍浙江绍兴。中央电视台、中国电视剧制作中心演员剧团国家一级演员，浙江大学人文学院兼职教授。六小龄童塑造的孙悟空一角，一直以来为广大西游记爱好者津津乐道。
                            </div>
                        </Box>
                        <Box className='block'>
                            <div>
                                <img src="/img/tutor-3.png" alt="" />
                            </div>
                            <div className='name'>柯以敏</div>
                            <div className='brief'>
                                亚洲最美女声- 柯以敏，1972年3月30日出生于马来西亚槟城，马来西亚华裔女歌手、演员，毕业于英国皇家音乐学院。1991年参加“亚洲之声”歌唱比赛，以一首《The power of love》获得总决赛亚军。1997年，推出第五张个人音乐专辑《邻居的耳朵》
                            </div>
                        </Box>
                    </Box>


                    <Box className='line'>
                        <Box className='block'>
                            <div>
                                <img src="/img/tutor-4.png" alt="" />
                            </div>
                            <div className='name'>朱桦</div>
                            <div className='brief'>
                                魅影歌后- 朱桦，中国内地歌手、音乐人。擅长民族、流行、古典、新世纪等演唱风格。 中国的PopDiva，素有“音乐洁癖”之称．因为她的声乐造诣，已经被邀请出任《超级女声》《快乐男声》《绝对唱响》《加油，好男儿》《联盟歌会》等多个重要比赛的评委，知性专业的点评与优雅的气质赢得了观众的良好口碑
                            </div>
                        </Box>
                        <Box className='block'>
                            <div>
                                <img src="/img/tutor-5.png" alt="" />
                            </div>
                            <div className='name'>文章</div>
                            <div className='brief'>
                                金钟金曲歌王- 文章本名黄文章，以名字为艺名，活跃于1980年代的台湾歌手，印尼华侨，祖籍中国广东。所会语言广及英语、日语、印尼语、马来语、闽南语、粤语、潮州话。他演唱的《古月照今尘》把中国人心中的历史情节，中华情节表现得厚重透彻，88年排名台湾榜首长达三十周。
                            </div>
                        </Box>
                    </Box>


                </Box>


                <Box className='section8'>
                    <div className='row-margin2'>
                        <img src="/img/singer2-10.png" alt="" />
                    </div>

                    <Box className='row row-margin2'>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-1.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-2.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-3.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-4.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-5.png" alt="" />
                        </div>
                    </Box>

                    <Box className='row row-margin2'>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-6.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-7.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-8.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-9.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-10.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row'>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-11.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-12.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-13.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-14.png" alt="" />
                        </div>
                        <div className='colum5'>
                            <img src="/img/singer2-group2-15.png" alt="" />
                        </div>
                    </Box>
                </Box>

                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer2-11.png" alt="" />
                    </div>
                    <div className='abs-container title1 flex-end font-light c-white'>
                        The Singer 2018 meeting event
                    </div>
                </Box>


                <Box className='section1 row'>
                    <div className='colum2'>
                        <img src="/img/singer2-12.png" alt="" />
                    </div>
                    <div className='colum2 flex-center'>
                        <div className='content7'>我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福</div>
                    </div>
                </Box>


                <Box>
                    <Box className='row row-margin2'>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-1.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-2.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-3.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-4.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin2'>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-5.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-6.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-7.png" alt="" />
                        </div>
                        <div className='colum4-2'>
                            <img src="/img/singer2-group3-8.png" alt="" />
                        </div>
                    </Box>

                </Box>

                <Box className='bg-grey c-white'>
                    <Box className='describe flex-center'>
                        <Box className='content7'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                        </Box>

                    </Box>
                </Box>

                <Box className='white-block2'></Box>


                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer2-13.png" alt="" />
                    </div>

                    <div className='abs-container title1 font-light c-white'>
                        The Singer 2018 final
                    </div>
                </Box>


                <Box className='section3 content7 describe'>
                    2018「唱响星途 」全球华人流行歌手大赛是2018年度最具规模和影响力的 海外华人文化活动之一。由澳大利亚申亿传媒主办 ，中国北京环球天音文化传媒有限公司, 日本晟株式会社，美国纽约国际艺术院，加拿大国家电视台，澳大利亚环澳传媒协办 ，汇聚全球五大洲顶尖华人新锐流行歌手，于2018年 7月8日齐聚澳大利亚悉尼 The Star 演艺中心，为全球华人献上一场精彩绝伦的视听盛宴，同时决出 2018年度全球华人 流行歌手 冠亚季军。本次大赛评委席将邀请中国华语乐坛/影视界五位顶级明星亲临现场，以最专业的角度及最具权威的评审标准，对所有决赛选手进行指导及点评。总决赛之夜，明星导师将和来自全球各国的参赛选手同台献艺，悉尼星港城必星光熠熠。
                </Box>

                <Box>
                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/singer2-group4-1.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer2-group4-2.png" alt="" />
                        </div>

                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/singer2-group4-3.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer2-group4-4.png" alt="" />
                        </div>
                    </Box>

                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-5.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-6.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-7.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-8.png" alt="" />
                        </div>
                    </Box>

                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-9.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-10.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-11.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-12.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-13.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-14.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-15.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer2-group4-16.png" alt="" />
                        </div>
                    </Box>
                </Box>

            </Box>

            <Album />
            <Footer />
        </div>
    )
}
