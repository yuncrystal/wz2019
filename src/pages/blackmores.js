import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import { Link } from "gatsby"
import "./singer.scss";


export default () => {
    return (
        <div>
            <Header title={'BLACKMORES'} img={'/img/bm-banner.png'} />

            <Box className='container singer'>
                <Box className='section3 content7 describe'>
                    Blackmores澳佳宝冠名2018唱响星途全球华人流行歌手大赛，WZ Media申亿传媒为澳佳宝量身打造了一系列线上线下活动以及比赛现场的品牌宣传活动。此次造星⼯厂之旅，由WZ Media申亿传媒组织的唱响星途全球⼗二强选手参观澳佳宝⼯厂，听澳佳宝历史和产品讲座，与澳佳宝负责人进⾏互动游戏。同时，WZ Media申亿传媒为澳佳宝策划了一系列与大赛相关的曝光活动，品牌形象也与⼤赛宣传同步进行，为澳佳宝产⽣ 了上千万次的曝光量。
                </Box>

                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/bm-1.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-2.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-3.png" alt="" />
                    </div>
                </Box>

                <div className=''>
                    <img src="/img/bm-4.png" alt="" />
                </div>

                <Box className='bg-grey c-white'>
                    <Box className='flex-center describe'>
                        <Box className='content7'>
                            此次造星⼯厂之旅，由WZ Media申亿传媒组织的 唱响星途全球⼗二强选手参观澳佳宝⼯厂，听澳佳宝历史和产品讲座，与澳佳宝负责⼈人进⾏互动游戏。
                        </Box>

                    </Box>
                </Box>


                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/bm-5.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-6.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-7.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/bm-8.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-9.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bm-10.png" alt="" />
                    </div>
                </Box>
            </Box>

            <Album />
            <Footer />

        </div>
    )
}