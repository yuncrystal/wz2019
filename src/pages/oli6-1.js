import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import "./singer.scss";


export default () => {
    return (
        <div>
            <Header title={'Oli6'} img={'/img/oli6-banner.png'} />

            <Box className='container singer'>
                <Box className='section3 content7 describe'>
                    WZ Media 申亿传媒在Oli6颖睿墨尔本的牧场举办了寻找羊咩咩的的活动，许多在墨尔本定居的华人家庭都来到了Oli6的牧场。小朋友们与小羊宝宝一起玩耍，给它们喂奶，挑选自己喜欢的小羊赛跑。看到广阔的草原和自由奔跑的山羊们，就能了解到Oli6对原材料天然健康的要求
                </Box>

                <Box className='row row-margin'>
                    <div>
                        <img src="/img/oli6-1.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/oli6-2.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/oli6-3.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/oli6-4.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/oli6-5.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/oli6-6.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/oli6-7.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/oli6-8.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/oli6-9.png" alt="" />
                    </div>
                </Box>
            </Box>

            <Album />
            <Footer />

        </div>
    )
}