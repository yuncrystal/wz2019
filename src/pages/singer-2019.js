import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import { Link } from "gatsby"
import "./singer.scss";

export default () => {
    return (
        <div>
            <Header titleClass={'title-mid'} title={'The Singr 2019'} img={'/img/singer3-banner.png'} />
            <Box className='container singer'>
                <div className='row years'>
                    <Link to="/singer-2017" className='item title1'>2017</Link>
                    <Link to="/singer-2018" className='item title1'>2018</Link>
                    <div className='item title1 active'>2019</div>
                </div>

                <Box className='section2 row'>
                    <div className='colum2'>
                        <img src="/img/singer3-1.png" alt="" />
                    </div>
                    <div className='colum2 flex-center'>
                        <div className='title half-content content7'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                        </div>
                    </div>
                </Box>

                <div>
                    <img src="/img/singer3-2.png" alt="" />
                </div>

                <Box className='section3 content7 describe'>
                    我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                </Box>

                <Box className='section4 pc-show'>
                    <Box className='row'>
                        <div className='colum4'>
                            <img src="/img/singer3-3.png" alt="" />
                            <div className='content7 tutor-brief'>
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                            </div>
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-4.png" alt="" />
                            <div className='content7 tutor-brief'>
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                            </div>
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-5.png" alt="" />
                            <div className='content7 tutor-brief'>
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                            </div>
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-6.png" alt="" />
                            <div className='content7 tutor-brief'>
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                            </div>
                        </div>
                    </Box>

                </Box>

                <Box className='section4 mobile-show tutor-card-container'>
                    <div className='tutor-card'>
                        <img src="/img/singer3-3-xs.png" alt="" />
                        <div className='content7 tutor-brief'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            </div>
                    </div>

                    <div className='tutor-card'>
                        <img src="/img/singer3-4-xs.png" alt="" />
                        <div className='content7 tutor-brief'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            </div>
                    </div>

                    <div className='tutor-card'>
                        <img src="/img/singer3-5-xs.png" alt="" />
                        <div className='content7 tutor-brief'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            </div>
                    </div>

                    <div className='tutor-card'>
                        <img src="/img/singer3-6-xs.png" alt="" />
                        <div className='content7 tutor-brief'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            </div>
                    </div>
                </Box>

                <Box className='section5 row'>
                    <div><img src="/img/singer3-11.png" alt="" /></div>
                    <div className='abs-container title1 flex-end font-light c-white'>
                        The Singer 2018 press conference
                    </div>
                </Box>


                <Box className='bg-grey c-white'>
                    <Box className='flex-center describe'>
                        <Box className='content7'>
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                            我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站
                        </Box>

                    </Box>
                </Box>

                <Box className='white-block'>

                </Box>

                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer3-12.png" alt="" />
                    </div>
                    <div className='abs-container c-black title1 flex-end font-light'>
                        The Singer 2018 tutor event
                    </div>
                </Box>

                <Box className='section3 content7 describe'>
                    我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                </Box>


                <Box className='section8'>
                    <Box className='row row-margin'>
                        <div className=''><img src="/img/singer3-13.png" alt="" /></div>
                    </Box>

                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer3-group-1.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-2.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-3.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-4.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer3-group-5.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-6.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-7.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-8.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row'>
                        <div className='colum4'>
                            <img src="/img/singer3-group-9.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-10.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-11.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group-12.png" alt="" />
                        </div>
                    </Box>


                </Box>


                <Box className='section5 row'>
                    <div>
                        <img src="/img/singer3-14.png" alt="" />
                    </div>
                    <div className='abs-container title1 font-light c-white'>
                        The Singer 2018 final
                    </div>
                </Box>

                <Box className='section3 content7 describe'>
                    我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                                我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字我是站位文字
                </Box>

                <Box className='section7'>
                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/singer3-group2-1.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer3-group2-2.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/singer3-group2-3.png" alt="" />
                        </div>
                        <div className='colum2'>
                            <img src="/img/singer3-group2-4.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-5.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-6.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-7.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-8.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-9.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-10.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-11.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-12.png" alt="" />
                        </div>
                    </Box>
                    <Box className='row'>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-13.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-14.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-15.png" alt="" />
                        </div>
                        <div className='colum4'>
                            <img src="/img/singer3-group2-16.png" alt="" />
                        </div>
                    </Box>
                </Box>
            </Box>

            <Album />
            <Footer />
        </div>
    )
}
