import React, { useState, useRef, useEffect } from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import { Link } from 'gatsby';
import "./projects.scss";



const projects = [
    {
        link: 'oli6-2',
        title: 'Oli6',
        content: 'Marketing / online campaign'
    },
    {
        link: '',
        title: 'Butler Organic',
        content: 'Branding'
    },
    {
        link: 'king-tea',
        title: 'King tea x Butler organics',
        content: 'Marketing / online campaign'
    },
    {
        link: 'singer-2017',
        title: 'The Singer 2017-2019',
        content: 'Event'
    },
    {
        link: 'singer-2018',
        title: 'The Singer 2017-2019',
        content: 'Event'
    },
    {
        link: 'singer-2019',
        title: 'The Singer 2017-2019',
        content: 'Event'
    },
    {
        link: 'bayer',
        title: 'Blackmores',
        content: 'Marketing / Event Promotion'

    },
    {
        link: 'bayer',
        title: 'Bayer',
        content: 'CBME Exhibiton'

    },
]


const ImageItem = ({ url, link, title, content}) => {
    return (
        <Link to={'/' + link} className='colum2 row-margin'>
            <div><img src={url} alt="" /></div>
            <div className='abs-container flex-v-end c-white title2 font-light'>
                <div className='modal'>
                    <div>{title}</div>
                    <div>{content}</div>
                </div>
            </div>
        </Link>
    )
}
const SliderItem = ({ start, end, refList }) => {
    const items = [];
    for (let i = start; i <= end; i++) {
        items.push(<ImageItem key={i} link={projects[i - 1].link} title={projects[i - 1].title} content={projects[i - 1].content} url={"/img/project-" + i + ".png"} />);
    }
    return (
        <Box className='album-column-2' ref={refList}>
            {
                items.map((item) => {
                    return item;
                })
            }
        </Box>
    )
}



export default () => {

    const refList = useRef(null);
    const [start, setStart] = useState(1);
    const [end, setEnd] = useState(4);
    const [loadMore, setLoadMore] = useState(true);

    const handleScroll = () => {
        // console.log('scrollHeight:',refList.current.scrollHeight)
        // console.log('offsetTop:',refList.current.offsetTop)
        // console.log('offset:',window.scrollY)
        if (window.scrollY > refList.current.scrollHeight) {
            setTimeout(() => {
                setEnd(8);
                setLoadMore(false)
            }, 2000)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    });

    return (
        <div>
            <Header title={'Projects'} img={'/img/projects-banner.png'} />

            <Box className='container project'>
                <Box className='tags-line'>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                    <div className='tag content4'>tag</div>
                </Box>


                <SliderItem refList={refList} start={start} end={end} />
                <Box display={loadMore ? 'flex' : 'none'} className='circularProgress'>
                    <span></span>
                    <span></span>
                    <span></span>
                </Box>

            </Box>
            <Footer />
        </div>
    )
}
