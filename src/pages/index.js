import React from "react"
import Header from '../components/headerMarket';
import "./index.scss";
import Box from '@material-ui/core/Box';
import Slider from "react-slick";
import ContactUs from '../components/contactus';
import Footer from '../components/footer';
import { Link } from "gatsby"

const ClintBlock = ({ img, position, text }) => {

    return (
        <Box className='block'>
            <div className='avator'>
                <img src={img} alt="" />
            </div>
            <div className='right'>
                <div className='content2 speak'>{text}</div>
                <div className='content3 position'>{position}</div>
            </div>

        </Box>
    )
}

export default () => {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        pauseOnHover: true,
        // autoplay: true,
    };

    return (
        <div>
            <Header img={'/img/marketing-banner.png'} />
            <div className='about-container container'>
                <div className='about'>
                    <div className='title1 center c-black'>About</div>
                    <div className='content'>
                        <img className='wz-logo' src="/img/wz-logo.png" alt="wz-logo.png" />
                        <div>
                            <div className='description content1'>
                                WZ Media Shenyi Media is a media culture company headquartered in Sydney, Australia. The
            company’s main business covers: media  promotion, cultural performance, marketing planning. He has
            successfully hosted the 2017 Singing Stars China-Australia Chinese Pop Singer Contest and the 2018
            Singing Stars Global Chinese Pop Singer Contest, a well-known media and cultural company in China
            and Australia. At the same time, WZ Media Shenyi Media has undertaken many online promotion of
            brands, offline activities, and achieved a lot of activities for various brands.
                    </div>
                        </div>

                    </div>
                    <Box display='flex' className='line1-container'>
                        <div className='line1'></div>
                    </Box>
                </div>
                <Box className='album  pc-show'>
                    <Box className='row row-margin'>
                        <div>
                            <img src="/img/marketing-1.png" alt="" />
                        </div>
                        <div className='abs-container flex-center'>
                            <div className='c-white title1 font-light'>Featured Projects</div>
                        </div>
                    </Box>
                    <Box className='row row-margin'>
                        <Link to="/oli6-1" className='colum2'>
                            <img src="/img/marketing-2.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Oli6</div>
                                    <div>Marketing / online campaign</div>
                                </div>

                            </div>
                        </Link>
                        <Link to="/blackmores" className='colum2'>
                            <img src="/img/marketing-3.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Blackmores</div>
                                    <div>Marketing / Event Promotion</div>
                                </div>
                            </div>
                        </Link>
                    </Box>
                    <Box className='row'>
                        <Link to="/singer-2019" className='colum3'>
                            <img src="/img/marketing-4.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>The Singer </div>
                                    <div>2017-2019 Event</div>
                                </div>
                            </div>
                        </Link>

                        <div className='colum3'>
                            <img src="/img/marketing-kotia.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Kotia</div>
                                    <div>Brand maintian</div>
                                </div>
                            </div>
                        </div>
                        <div className='colum3'>
                            <img src="/img/marketing-6.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Butler Organic</div>
                                    <div>Branding</div>
                                </div>
                            </div>
                        </div>
                    </Box>
                </Box>

                <Box className='mobile-show-v2'>
                    <Box className='title1 c-black center'>Featured Projects</Box>

                    <Slider {...settings}>

                        <Link to='/singer-2017' className='project-block'>
                            <img src="/img/marketing-2-xs.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>The Singer </div>
                                    <div>2017-2019 Event</div>
                                </div>
                            </div>
                        </Link>
                        <Link to='/oli6-1' className='project-block'>
                            <img src="/img/marketing-3-xs.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Oli6</div>
                                    <div>Marketing / online campaign</div>
                                </div>
                            </div>
                        </Link>

                        <Box className='project-block'>
                            <img src="/img/marketing-4-xs.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Kotia</div>
                                    <div>Brand maintian</div>
                                </div>
                            </div>
                        </Box>
                        <Box className='project-block'>
                            <img src="/img/marketing-5-xs.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Butler Organic</div>
                                    <div>Branding</div>
                                </div>
                            </div>
                        </Box>

                        <Link to='/blackmores' className='project-block'>
                            <img src="/img/marketing-6-xs.png" alt="" />
                            <div className='abs-container flex-v-end c-white title2 font-light'>
                                <div className='modal'>
                                    <div>Blackmores</div>
                                    <div>Marketing / Event Promotion</div>
                                </div>
                            </div>
                        </Link>
                    </Slider>

                    <Box className='flex-center'>
                        <Link className='find-btn flex-center' to="/projects">Find out more</Link>
                    </Box>



                </Box>


                <Box className='help-container'>
                    <Box className='title'>
                        <div className='title1 center c-black'>We are here to help</div>
                    </Box>

                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/marketing-7.png" alt="" />
                            <div className='abs-container flex-center c-white title3 font-light'>
                                <div className='modal'>
                                    <img className='icon' src="/img/icon/icon1.png" alt="" />
                                    <div>Marketing</div>
                                    <div>Strategy</div>
                                </div>
                            </div>

                        </div>
                        <div className='colum2'>
                            <img src="/img/marketing-8.png" alt="" />
                            <div className='abs-container flex-center c-white title3 font-light'>
                                <div className='modal'>
                                    <img className='icon' src="/img/icon/icon2.png" alt="" />
                                    <div>Event</div>
                                    <div>Promotion</div>
                                </div>
                            </div>
                        </div>
                    </Box>

                    <Box className='row row-margin'>
                        <div className='colum2'>
                            <img src="/img/marketing-9.png" alt="" />
                            <div className='abs-container flex-center c-white title3 font-light'>
                                <div className='modal'>
                                    <img className='icon' src="/img/icon/icon3.png" alt="" />
                                    <div>Creative</div>
                                    <div>Design</div>
                                </div>
                            </div>
                        </div>
                        <div className='colum2'>
                            <img src="/img/marketing-10.png" alt="" />
                            <div className='abs-container flex-center c-white title3 font-light'>
                                <div className='modal'>
                                    <img className='icon' src="/img/icon/icon4.png" alt="" />
                                    <div>Video</div>
                                </div>
                            </div>
                        </div>
                    </Box>
                </Box>

                <Box className='client'>
                    <div className='title title1 center'>Our Clients</div>


                    <Slider className='slider-container' {...settings}>
                        <ClintBlock
                            img={"/img/client-2.png"}
                            position={'—Kotia CEO'}
                            text={'They listened to our requests and often went above and beyond to provide what we need.'} />

                        <ClintBlock
                            img={"/img/client-3.png"}
                            position={'—Butler Organics CEO'}
                            text={'Their responsiveness and an ability to overcome challenges with ease are highlights of their work. '} />

                        <ClintBlock
                            img={"/img/client-4.png"}
                            position={'—Jim&Jud CEO'}
                            text={'Bringing their creativity and attention to detail to bear, they both heard my ideas and contributed their own.'} />
                        <ClintBlock
                            img={"/img/client-1.png"}
                            position={'—Oli6 CEO'}
                            text={'They are great project managers. They regularly informed me of their progress, and I could follow a timetable to see exactly what was happening every day.'} />
                    </Slider>

                </Box>

                <Box className='supports'>

                    <div className='title8 c-grey2 mobile-show-v2 center'>Cooperative Sponsor</div>

                    <Box className='row supports-row'>
                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/creation.png" alt="" />
                        </div>

                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/mingtian.png" alt="" />
                        </div>

                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/haneco.png" alt="" />
                        </div>

                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/natures-are.png" alt="" />
                        </div>

                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/PARISI.png" alt="" />
                        </div>

                        <div className='colum6-v2'>
                            <img className='colum6-v2-img' src="/img/brand/blackmores.png" alt="" />
                        </div>
                    </Box>

                    <div className='title8 c-grey2 mobile-show-v2 center'>Strategic Partners</div>

                    <Box className='row line-2'>
                        <div className='colum4-nospace'>
                            <img src="/img/brand/qixiu.png" alt="" />
                        </div>

                        <div className='colum4-nospace'>
                            <img src="/img/brand/QIY.png" alt="" />
                        </div>

                        <div className='colum4-nospace'>
                            <img src="/img/brand/weibo.png" alt="" />
                        </div>

                        <div className='colum4-nospace'>
                            <img src="/img/brand/yi.png" alt="" />
                        </div>
                    </Box>
                </Box>
            </div>

            <ContactUs />

            <Footer />


        </div>
    )
}
