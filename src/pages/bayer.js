import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import { Link } from "gatsby"
import "./singer.scss";


export default () => {
    return (
        <div>
            <Header title={'BAYER'} img={'/img/bayer-banner.png'} />

            <Box className='container singer'>
                <Box className='section3 content7 describe'>
                   我似乎展开的肌肤善良的肌肤抗衰老的家
                </Box>


                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/bayer-1.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/bayer-2.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/bayer-3.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/bayer-4.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/bayer-5.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/bayer-6.png" alt="" />
                    </div>
                </Box>

                <Box className='row row-margin'>
                    <div className='colum3'>
                        <img src="/img/bayer-7.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bayer-8.png" alt="" />
                    </div>
                    <div className='colum3'>
                        <img src="/img/bayer-9.png" alt="" />
                    </div>
                </Box>
            </Box>

            <Album />
            <Footer />

        </div>
    )
}