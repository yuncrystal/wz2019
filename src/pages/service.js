import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import "./service.scss";

const ImageItem = ({ text1, text2, url,icon }) => {
    return (
        <div className='colum1'>
            <img src={url} alt="" />
            <div className='abs-container flex-center c-white title3 font-light'>
                <div className='modal'>
                    <img className='icon' src={icon} alt="" />
                    <div>{text1}</div>
                    <div>{text2}</div>
                </div>
            </div>
        </div>
    )
}

export default () => {
    return (
        <div>
            <Header title={'Service'} img={'/img/service-banner.png'} />
            <Box className='container service'>
                <Box className='describe content7'>
                        dfsfdkslfsldkjfksljfsdljfklsd
                </Box>
                <Box>
                    <ImageItem text1={"Marketing"} text2={'Strategy'} icon={'/img/icon/icon1.png'} url={'/img/service-1.png'}/>
                    <ImageItem text1={"Event"} text2={'Promotion'} icon={'/img/icon/icon2.png'} url={'/img/service-2.png'}/>
                    <ImageItem text1={"Creative"} text2={'Desigh'} icon={'/img/icon/icon3.png'} url={'/img/service-3.png'}/>
                    <ImageItem text1={"Video"} text2={''} icon={'/img/icon/icon4.png'} url={'/img/service-4.png'}/>
                </Box>
                
            </Box>
            <Footer />
        </div>
    )
}
