import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Album from '../components/album';
import Box from '@material-ui/core/Box';
import { Link } from "gatsby"
import "./singer.scss";

export default () => {
    return (
        <div>
            <Header titleClass={'title-mid'} title={'The Singr 2017'} img={'/img/singer1-banner.png'} />
            <Box className='container singer'>
                <div className='row years'>
                    <div className='item title1 active'>2017</div>
                    <Link to="/singer-2018" className='item title1'>2018</Link>
                    <Link to="/singer-2019" className='item title1'>2019</Link>
                </div>
                <div>
                    <img src="/img/singer1-1.png" alt="" />
                </div>
                <div className='describe bg-grey content7 c-white font-light'>
                    The Singer唱响星途--中澳华人流行歌手大赛是由WZ申亿传媒联合T.I.P宫传媒强力打造的大型专业音乐比赛，旨在挖掘和培养有真正实力的，有潜力的，热爱唱歌的中澳华人，并且邀请中国最具实力和影响力的明星导师直接面对面选拔和指导，助推具有音乐梦想的中澳歌者踏上星途 。
                </div>

                <Box className='section1 row'>
                    <div className='colum2'>
                        <img src="/img/singer1-2.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <div className='title title4'>The Singer 2018 press conference</div>
                        <div className='content7'>我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福我是站位福</div>
                    </div>
                </Box>
                <Box className='row row-margin'>
                    <div className='colum2'>
                        <img src="/img/singer1-3.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/singer1-4.png" alt="" />
                    </div>
                </Box>
                <Box className='row'>
                    <div className='colum2'>
                        <img src="/img/singer1-5.png" alt="" />
                    </div>
                    <div className='colum2'>
                        <img src="/img/singer1-6.png" alt="" />
                    </div>
                </Box>

            </Box>

            

            <Album />

            <Footer />
        </div>
    )
}
