import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '@material-ui/core/Box';
import Album from '../components/album';
import "./singer.scss";


export default () => {
    return (
        <div>
            <Header title={'Oli6'} img={'/img/oli6-banner.png'} />

            <Box className='container singer'>
                <Box className='section3 content7 describe'>
                    Oli6颖睿羊奶粉 悉尼Brand day：Oli6颖睿羊奶粉委托WZ Media 申亿传媒在悉尼成功举办了一场寻找羊咩咩主题的品牌活动日，让澳洲的华人家庭对Oli6颖睿羊奶粉有更深入的了解。同时，现场设置了许多儿童游乐区，让小朋友们一起快乐地玩耍。为Oli6刚出生的小羊取名的环节更是激起了所有人的热情，还有幸运家庭被抽取到了全家免费到Oli6墨尔本牧场参观游玩的旅行。
                </Box>

                <Box className='row pc-show'>
                    <Box className='colum-left'>
                        <div className='column-margin'>
                            <img src="/img/oli6-3-1.png" alt="" />
                        </div>
                        <div>
                            <img src="/img/oli6-3-2.png" alt="" />
                        </div>
                    </Box>

                    <Box className='colum-right'>
                        <div className='column-margin'>
                            <img src="/img/oli6-3-3.png" alt="" />
                        </div>
                        <div>
                            <img src="/img/oli6-3-4.png" alt="" />
                        </div>
                    </Box>
                </Box>

                <Box className='mobile-show-v2 mobile-container'>
                    <div className='row column-margin'>
                        <div><img src="/img/oli6-3-1.png" alt="" /></div>
                    </div>

                    <div className='row flex'>
                        <div className='colum3 '>
                            <img src="/img/oli6-3-2.png" alt="" />
                        </div>
                        <div className='colum3 '>
                            <img src="/img/oli6-3-3.png" alt="" />
                        </div>
                        <div className='colum3 '>
                            <img src="/img/oli6-3-4.png" alt="" />
                        </div>
                    </div>
                </Box>
            </Box>
            <Album />
            <Footer />

        </div>
    )
}