import React from "react"
import Header from '../components/header';
import Footer from '../components/footer';
import ContactUs from '../components/contactus';

export default () => {
    return (
        <div>
            <Header title={'Contact US'} img={'/img/contact-banner.png'} />
            
            <ContactUs />
            
            <Footer />
        </div>
    )
}
