// const React = require("react")

require('slick-carousel/slick/slick.css')
require('slick-carousel/slick/slick-theme.css')

require('./src/styles/global.scss')
require('./src/styles/global-xs.scss')

// const Layout = require("./src/components/layout")
// Logs when the client route changes
// exports.onRouteUpdate = ({ location, prevLocation }) => {
//     console.log("new pathname", location.pathname)
//     console.log("old pathname", prevLocation ? prevLocation.pathname : null)
// }
// // Wraps every page in a component
// exports.wrapPageElement = ({ element, props }) => {
//     return <Layout {...props}>{element}</Layout>
// }
